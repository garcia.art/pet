const routes = (module.exports = require('next-routes')());

routes.add('/', 'Home');
routes.add('/precos', 'Pricing');
routes.add('/loading', 'Loading');
