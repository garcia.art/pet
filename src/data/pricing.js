import Price1 from '../public/price1.svg';
import Price2 from '../public/price2.svg';
import Price3 from '../public/price3.svg';

export const pricing = [
    {
        src: Price1,
        title: 'Essencial',
        price: '89',
        content: (
            <>
                <div>
                    <p>1 usuário</p>
                </div>
                <div>
                    <p>Cadastros e Relatórios</p>
                </div>
                <div>
                    <p>Agenda de Serviços</p>
                </div>
                <div>
                    <p>Controle de Estoque</p>
                </div>
                <div>
                    <p>Comandas</p>
                </div>
                <div>
                    <p>Contas a Pagar e Receber</p>
                </div>
                <div>
                    <p>Módulo de Banho e Tosa</p>
                </div>
                <div>
                    <p>Módulo de Clínica Veterinária</p>
                </div>
            </>
        )
    },
    {
        src: Price2,
        title: 'Professional',
        price: '199',
        content: (
            <>
                <div>
                    <p>até 3 usuários</p>
                </div>
                <div>
                    <p>
                        <strong>Recursos do Essencial +</strong>
                    </p>
                </div>
                <div>
                    <p>Cálculo de Comissões</p>
                </div>
                <div>
                    <p>Controle de Estoque</p>
                </div>
                <div>
                    <p>Comandas</p>
                </div>
                <div>
                    <p>Contas a Pagar e Receber</p>
                </div>
                <div>
                    <p>Módulo de Banho e Tosa</p>
                </div>
                <div>
                    <p>Módulo de Clínica Veterinária</p>
                </div>
            </>
        )
    },
    {
        src: Price3,
        title: 'Premium',
        price: '289',
        content: (
            <>
                <div>
                    <p>até 5 usuários</p>
                </div>
                <div>
                    <p>
                        <strong>Recursos do Professional +</strong>
                    </p>
                </div>
                <div>
                    <p>Aplicativo para Clientes</p>
                </div>
                <div>
                    <p>Módulo Fiscal(NFC-e online)</p>
                </div>
                <div>
                    <p>Módulo de Internação</p>
                </div>
            </>
        )
    }
];
