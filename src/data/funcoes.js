import Service1 from '../public/service1.svg';
import Service2 from '../public/service2.svg';
import Service3 from '../public/service3.svg';
import Service4 from '../public/service4.svg';
import Service5 from '../public/service5.svg';
import Service6 from '../public/service6.svg';
import Service7 from '../public/service7.svg';

export const funcoes = [
    {
        src: Service1,
        h4: 'Melhoria no Atendimento',
        p:
            'A melhor interface entre seu negócio e o cliente. Conte com a praticidade de ter todas as informações em um só lugar.'
    },
    {
        src: Service2,
        h4: 'Agendamento de  Serviços',
        p:
            'No ControlePet você tem uma agenda que te auxilia a manter uma excelente organização de horários e serviços.'
    },
    {
        src: Service3,
        h4: 'Aplicativo Mobile',
        p:
            'Através do aplicativo Mobile o cliente pode acessar e receber diversas informações sobre o seu Pet.'
    },
    {
        src: Service4,
        h4: 'Gestão Financeira Completa',
        p:
            'Controle as suas contas correntes, cartões de crédito, realize transferências entre contas e lançamentos financeiros.'
    },
    {
        src: Service6,
        h4: 'Emissão de Nota Fiscal',
        p:
            'O módulo fiscal do ControlePet possibilita, de forma facilitada e consciente, a emissão de Nota Fiscal Eletrônica do Consumidor.'
    },
    {
        src: Service7,
        h4: 'Internação',
        p:
            'Melhore e profissionalize ainda mais suas internações. Controle e acompanhamento do fluxo de informações sobre a internação.'
    }
];
