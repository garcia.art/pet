import Client1 from '../public/Client1.svg';
import Client2 from '../public/Client2.svg';
import Client3 from '../public/Client3.svg';

export const client = [
    {
        p:
            '“Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt omnis iste natus. ”',
        src: Client1,
        h3: 'Elva Hart',
        h4: 'CEO/Letters Inc'
    },
    {
        p:
            '“Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt omnis iste natus. ”',
        src: Client2,
        h3: 'Anna Rebens',
        h4: 'Co Founder/Company'
    },
    {
        p:
            '“Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt omnis iste natus. ”',
        src: Client3,
        h3: 'Roberta Sanders',
        h4: 'CTO/Acneds'
    }
];
