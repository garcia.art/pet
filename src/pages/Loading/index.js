import React, { useEffect } from 'react';
import * as S from '../../style/Loading/styled';
import GlobalStyle from '../../style/global.js';

export default function Loading() {
    useEffect(() => {
        (function () {
            var loader = document.getElementById('myLoader');
            const dots = 3;
            const margin = 10;
            for (var i = 0; i < dots; i++) {
                var chtml = '<span></span>';
                loader.innerHTML += chtml;
            }
            const dot = loader.getElementsByTagName('span');
            for (var j = 0; j < dot.length; j++) {
                const dotWidth = dot[j].offsetWidth + margin;
                dot[j].style.left = dotWidth * j + 'px';
            }
            var tick = 0;
            var myTimer = setInterval(function () {
                for (var k = 0; k < dot.length; k++) {
                    dot[k].style.transform = 'scale(1, 1)';
                    dot[tick].style.transform = 'scale(1.2, 1.2)';
                }
                tick++;
                if (tick >= dot.length) {
                    tick = 0;
                }
            }, 200);
        })();
    }, []);

    return (
        <S.Wrapper>
            <GlobalStyle></GlobalStyle>
            <S.Loader>
                <div>
                    <div className="loader" id="myLoader"></div>
                </div>
                <h4>Carregando</h4>
            </S.Loader>
        </S.Wrapper>
    );
}
