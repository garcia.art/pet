import React from 'react';
import * as S from '../../style/Pricing/styled';
import Template from '../../Template/default';
import LPricing from '../../containers/LPricing';

export default function Pricing() {
    return (
        <Template title="ControlePet">
            <S.Wrapper>
                <LPricing></LPricing>
            </S.Wrapper>
        </Template>
    );
}
