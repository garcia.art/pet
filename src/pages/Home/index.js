import React from 'react';
import * as S from '../../style/Home/styled';
import Template from '../../Template/default';
import Landing from '../../containers/Landing/index.js';
import Funcoes from '../../containers/Funcoes/index.js';
import Clientes from '../../containers/Clientes/index.js';
import Teste from '../../containers/Teste/index.js';

export default function Home() {
    return (
        <Template title="ControlePet">
            <S.Wrapper>
                <Landing></Landing>
                <Funcoes></Funcoes>
                <Clientes></Clientes>
                <Teste></Teste>
            </S.Wrapper>
        </Template>
    );
}
