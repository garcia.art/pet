import React from 'react';
import Head from 'next/head';
import GlobalStyle from '../../style/global.js';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import * as S from './styled';
export default function Default({ children, title, ...props }) {
    return (
        <>
            <Head>
                <title>{title}</title>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            </Head>
            <S.Wrapper>
                <Header />
                <div>{children}</div>
                <Footer />
            </S.Wrapper>
            <GlobalStyle />
        </>
    );
}
