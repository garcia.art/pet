import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`

@import url('https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap');
 
 *{outline: none; }
  
 html {
  box-sizing: border-box;
}
  *, *:before, *:after {
  box-sizing: inherit;
}
  h1,h2,h3,h4,p{
    margin: 0;
  }
  
  h1{
    font-size: 1.8em;
    font-weight: 600;
  }

  h2{
    font-size: 1.6em;
  }
  
  h3{
    font-size: 1.4em;
  }

  h4{
    font-size: 1.2em;
  }

  p{
    font-size: 1em;
    font-weight: 300;
  }
  body {
    margin: 0;
    padding: 0;    
    font-family: 'Montserrat', sans-serif;
  }
  /* 
  palette: {
    primary: {
      main: '#7265E3',
      medium: '#8C80F8',
      soft: '#AF8EFF',
      ligth: '#E4DFFF',
    },
    secondary: {
      main: '#FF9B90'
    },
    grey: {
      main: '#2D3142',
      medium: '#4C5980',
      soft: '#9C9EB9',
      ligth: '#D6D9E0',
    },
    blue: {
      dark: '#172b4d'
    }
  
  */

    button, a, select{
      cursor: pointer;
      border: none;
    }

    textarea:focus, input:focus, select:focus {
    box-shadow: 0 0 0 0;
    border: 0 none;
    outline: 0;
} 
`;

export default GlobalStyle;
