import styled from 'styled-components';

export const Wrapper = styled.div`
    width: 100%;
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    background: #7265e3;
`;

export const Loader = styled.div`
    display: flex;
    justify-content: center;

    h4 {
        color: #fff;
        margin-top: 40px;
    }

    .loader {
        position: relative;
    }
    .loader span {
        position: absolute;
        width: 20px;
        height: 20px;
        background: #fff;
        border-radius: 50%;
        transition: transform 0.2s ease-out;
    }
`;
