import styled from 'styled-components';

export const Button = styled.button`
    -webkit-border-radius: 6px;
    -moz-border-radius: 6px;
    border-radius: 6px;
    font-size: 1.2rem;
    font-weight: 600;
    color: #fff;
    text-transform: uppercase;

    ${({ typeButton }) =>
        typeButton === 'home1' &&
        ` width:180px;
          height: 50px;
          background: linear-gradient(180deg, #FF9B90 182.14%, #F18478 282.14%);
          box-shadow: 0px 2px 37px #AF8EFF;
          margin: 1rem 0;

    @media (max-width: 250px){
      width: 100%;
    }
    @media (min-width: 800px) {
      width:200px;
          height:50px;
          margin-top: 2em;
    }
    
  `}

    ${({ typeButton }) =>
        typeButton === 'home2' &&
        ` width:200px;
          height: 50px;
          background: linear-gradient(180deg, #FF9B90 0%, #FFA298 100%);
    
         @media (max-width: 250px){
           width: 100%;
    }
    
  `}

${({ typeButton }) =>
        typeButton === 'price' &&
        ` width:200px;
          height: 50px;
          background: linear-gradient(180deg, #FF9B90 0%, #FFA298 100%);
          text-transform:capitalize;
          font-weight: 500;
          margin-top: 35px;
        
    
  `}
`;
