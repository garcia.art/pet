import React from 'react';
import * as S from './styled';

export default function Card({ children, ...props }) {
    return (
        <>
            <S.Card {...props}>{children}</S.Card>
        </>
    );
}
