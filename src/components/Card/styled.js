import styled from 'styled-components';

export const Card = styled.div`
    display: flex;
    flex-direction: column;
    text-align: center;
    align-items: center;
    background: #fff;

    ${({ typeCard }) =>
        typeCard === 'home' &&
        `
    height: 250px;
    width: 100%;
    padding: 0 1em; 
    justify-content: center;
h4{
font-weight: 600;
text-align: center;
color: #333333;
margin-bottom:1em;

}

p{font-weight: 300;
font-size: 1em;
line-height: 1.1em;
text-align: center;
color: #676F79;
}

img{
    margin-bottom:1em;
    height:45px;
}

    
    @media (min-width: 1000px) {
        width: 350px;
        height: 300px;
        margin-bottom: 0px;
        p{
margin-bottom:1em;
        
}
        
    }
  `}

    ${({ typeCard }) =>
        typeCard === 'clients' &&
        `
        
    width: 100%;
    max-height: 170px;
    padding: 2em;
    color: #676f79;
    border-radius: 10px;
    line-height: 1.9em;
    margin-bottom: 120px;
    margin-top: 30px;
    
    @media (min-width: 700px) {
        width: 50%;
        margin-top: 0px;
        
    }


    @media (min-width: 1000px) {
        width: 30%;
        margin-bottom: 0px;
        
    }

    
  `}


${({ typeCard }) =>
        typeCard === 'price' &&
        `
        width: 100%;
        height: 100%;
        max-width: 500px;
        border-radius:4px;
        padding: 2em;
        box-shadow: 0px 10px 50px rgba(0, 0, 0, 0.1);
        margin-bottom: 5em;
        img{
            height:45px;
        }

        h2{
            font-weight: normal;
            font-size: 1.85em;
            line-height: 2em;
            color: #333333;
        }
        
        div{
            border-bottom: 1px solid; 
            border-color: rgba(0, 0, 0, 0.1);;
            width: 100%;
            padding: 1em 0 0.5em 0;
        }
        
        div p{
            color: #676F79;
            font-weight: 300;
            line-height: 1.8em;
        }

        sup, span{
            text-transform: uppercase;
            color: #97A3B4;
            font-weight: 400;
            font-size: 0.4em;
            line-height: 26px;
            letter-spacing: 1.9px;

        }

        h3{
            font-weight: 600;
            font-size: 2.4em;
            line-height: 53px;
            color: #333333;
            margin:0.5em 0;
        }

        @media (min-width: 800px){
            max-width: 300px;
            margin-bottom: 1em;

        }

        @media (min-width: 1030px){
            max-width: 350px;

        }

   
  `}
`;
