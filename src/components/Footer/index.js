import React from 'react';
import * as S from './styled';
import Link from 'next/link';
// ------ Imports padrão
import Facebook from '../../public/facebook.svg';
import Instagram from '../../public/instagram.svg';
import LinkedIn from '../../public/linkedin.svg';
import Logo from '../../public/logo-orbit.svg';
import { Container } from '../../components/Container';
export default function Footer() {
    return (
        <S.Wrapper>
            <Container>
                <S.Top>
                    <S.Logo>
                        <Link href="/">
                            <img src={Logo} height="50px"></img>
                        </Link>
                    </S.Logo>
                    <S.Links>
                        <Link href="/">
                            <a>Home</a>
                        </Link>
                        <Link href="/precos">
                            <a>Preços</a>
                        </Link>
                        <Link href="/contato">
                            <a>Contato</a>
                        </Link>
                    </S.Links>
                </S.Top>

                <S.Bottom>
                    <S.SubBottom>
                        <S.SubContent>
                            <Link href="">Política de Privacidade</Link>
                            <Link href="">Termos de Serviço</Link>
                        </S.SubContent>

                        <S.Social>
                            <Link href="../facebook.com/controlePet">
                                <img src={Facebook}></img>
                            </Link>
                            <Link href="instagram.com/controlePet">
                                <img src={Instagram}></img>
                            </Link>
                            <Link href="linkedin.com/controlePet">
                                <img src={LinkedIn}></img>
                            </Link>
                        </S.Social>
                    </S.SubBottom>
                    <S.Copy>&copy; 2018 OrbitHub All Rights Reserved</S.Copy>
                </S.Bottom>
            </Container>
        </S.Wrapper>
    );
}
