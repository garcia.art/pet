import styled from 'styled-components';

export const Wrapper = styled.div`
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: space-between;
    flex-direction: column;
    background: #172b4d;
    position: relative;
    z-index: -1;
    bottom: 0;
    img {
        cursor: pointer;
    }
    a {
        text-decoration: none;
        color: #ffffff;
    }
`;

export const Top = styled.div`
    display: flex;
    flex-direction: column;

    @media (min-width: 1000px) {
        flex-direction: row;
        justify-content: space-between;
        width: 100%;
        height: 195px;
        border-bottom: 0.1px solid #fff;
        padding: 1em;
    }
`;
export const Bottom = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;

    @media (min-width: 1000px) {
        height: 83px;
        flex-direction: row-reverse;
        justify-content: space-between;
        align-items: center;
        width: 100%;
        padding: 1em;
    }
`;

export const Logo = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 2em 0;

    @media (min-width: 1000px) {
        margin: 0;
    }
`;

export const Links = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;

    a {
        font-weight: 400;
        text-transform: uppercase;
        font-style: normal;
        font-size: 1rem;
        letter-spacing: 2px;
        margin-bottom: 1.5rem;
    }

    @media (min-width: 1000px) {
        flex-direction: row;
        width: 30%;
        justify-content: space-between;

        a {
            margin-bottom: 0;
            font-size: 0.8rem;
        }
    }
`;
export const SubContent = styled.div`
    display: flex;
    justify-content: space-between;
    flex-direction: column;
    align-items: center;

    a {
        font-size: 0.8em;
        margin-bottom: 0.5rem;
    }

    @media (min-width: 1000px) {
        flex-direction: row;

        a {
            margin: 1em 0.5em 1em 0;
        }
    }
`;

export const SubBottom = styled.div`
    @media (min-width: 1000px) {
        display: flex;
        flex-direction: row;
    }
`;

export const Social = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin: 1em 1.5em;

    img {
        height: 1.2em;
    }

    @media (min-width: 1000px) {
        width: 80px;
        align-items: center;
        border-left: 0.05px solid #fff;
        padding: 0.5em;
        margin: 0;
    }
`;

export const Copy = styled.div`
    font-size: 0.6rem;
    color: #ffffff;
    margin-bottom: 2em;
    display: flex;
    align-items: center;
    justify-content: center;

    @media (min-width: 1000px) {
        margin: 0em;
    }
`;
