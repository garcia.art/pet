import styled from 'styled-components';

export const Wrapper = styled.div`
    width: 100%;
    height: 60px;
    position: fixed;
    background: #877bf5;
    color: #fff;

    img {
        cursor: pointer;
    }
    a,
    label {
        color: #fff;
    }
    a:hover,
    label:hover {
        color: #ff9b90;
    }
    @media (min-width: 1000px) {
        position: absolute;
    }
`;

export const Content = styled.div`
    width: 100%;
    height: 100%;
    padding: 0 1em;
    display: flex;
    justify-content: space-between;
    align-items: center;
`;

export const Logo = styled.div`
    height: 40px;
    img {
        height: 100%;
    }
`;

export const Menu = styled.div`
    .show-menu-btn,
    .hide-menu-btn {
        transition: 0.4s;
        font-size: 30px;
        cursor: pointer;
        display: none;
    }

    .menu a:hover,
    .show-menu-btn:hover,
    .hide-menu-btn:hover {
        color: #ff9b90;
    }

    #chk {
        position: absolute;
        visibility: hidden;
        z-index: -1000;
    }

    ul li {
        list-style: none;
        display: inline-flex;
    }
    ul li a {
        display: block;
        text-decoration: none;
        text-transform: uppercase;
        letter-spacing: 1.84615px;
    }

    @media (max-width: 700px) {
        .menu a {
            transition: 0.4s;
        }

        .show-menu-btn,
        .hide-menu-btn {
            display: block;
        }
        .menu {
            position: fixed;
            width: 100%;
            height: 100%;
            background: linear-gradient(27.66deg, #7265e3 -51.46%, #8c80f8 206.23%);
            right: -100%;
            top: 0;
            text-align: center;
            padding: 80px 0;
            transition: 0.7s;
            display: flex;
            flex-direction: column;
            align-items: center;
            margin-top: 0;
            z-index: 1000;
        }
        .menu a {
            display: block;
            padding: 20px;
        }
        .hide-menu-btn {
            position: absolute;
            top: 1em;
            right: 1em;
        }
        #chk:checked ~ .menu {
            right: 0;
        }
    }

    @media (min-width: 700px) {
        width: 40%;
        ul {
            display: flex;
            padding: 0;
            justify-content: space-between;
        }
    }
    @media (min-width: 1000px) {
        width: 30%;
    }
`;
