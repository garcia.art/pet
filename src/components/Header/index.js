import React from 'react';
import * as S from './styled.js';
import Link from 'next/link';
import Logo from '../../public/logo-orbit.svg';
import { menu } from '../../data/menu';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { Container } from '../Container/index.js';

export default function Header() {
    return (
        <S.Wrapper>
            <Container>
                <S.Content>
                    <S.Logo>
                        <Link href="/">
                            <img src={Logo}></img>
                        </Link>
                    </S.Logo>
                    <S.Menu>
                        <input type="checkbox" id="chk"></input>
                        <label htmlFor="chk" className="show-menu-btn">
                            <FontAwesomeIcon className="hamburguer" icon={faBars} />
                        </label>
                        <ul className="menu">
                            {menu.map(({ label, ref }) => (
                                <li>
                                    <Link href={ref}>
                                        <a>{label}</a>
                                    </Link>
                                </li>
                            ))}
                            <label htmlFor="chk" className="hide-menu-btn">
                                <FontAwesomeIcon icon={faTimes} />
                            </label>
                        </ul>
                    </S.Menu>
                </S.Content>{' '}
            </Container>
        </S.Wrapper>
    );
}
