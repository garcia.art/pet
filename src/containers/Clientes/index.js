import React from 'react';
import * as S from './styled.js';
import Card from '../../components/Card';
import { client } from '../../data/client';
import { Container } from '../../components/Container/index.js';

export default function Clientes() {
    return (
        <S.Wrapper>
            <Container>
                <S.Content>
                    <h1>O QUE OS CLIENTES FALAM DE NÓS</h1>
                    <p>
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
                        eu fugiat nulla pariatur.
                    </p>
                </S.Content>

                <S.CardWrapper>
                    {client.map(({ p, src, h3, h4 }) => (
                        <Card typeCard="clients">
                            <p>{p}</p>
                            <S.Client>
                                <S.Image>
                                    <img src={src}></img>{' '}
                                </S.Image>
                                <h3>{h3}</h3>
                                <h4>{h4}</h4>
                            </S.Client>
                        </Card>
                    ))}
                </S.CardWrapper>
            </Container>
        </S.Wrapper>
    );
}
