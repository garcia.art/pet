import styled from 'styled-components';

export const Wrapper = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    text-align: center;
    color: #fff;
    background: linear-gradient(324.13deg, #7265e3 10.35%, #8c80f8 96.3%);

    h1 {
        font-size: 1.8em;
        text-transform: uppercase;
        font-weight: 600;
        line-height: 2.1em;
    }
    p {
        font-size: 1em;
        line-height: 1.9em;
    }

    @media (min-width: 1000px) {
        min-height: 700px;
        justify-content: center;
        align-items: center;
        p {
            font-size: 1em;
            line-height: 1.9em;
        }
    }
`;

export const Content = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    text-align: center;
    justify-content: center;
    margin: 40px 0;
    p {
        width: 100%;
    }

    @media (min-width: 700px) {
        p {
            max-width: 60%;
        }
    }
    @media (min-width: 1000px) {
        margin-bottom: 2em;
        margin-top: -100px;

        p {
            max-width: none;
        }
    }
`;

export const CardWrapper = styled.div`
    @media (min-width: 700px) {
        display: flex;
        width: 100%;
        flex-direction: column;
        justify-content: space-between;
        align-items: center;
    }

    @media (min-width: 1000px) {
        display: flex;
        height: 200px;
        width: 100%;
        flex-direction: row;
        justify-content: space-between;
    }
`;

export const Client = styled.div`
    display: flex;
    position: relative;
    align-items: center;
    flex-direction: column;
    text-align: center;
    color: #ffffff;
    top: 80%;

    h3 {
        font-weight: 600;
        font-size: 1em;
        margin: 0;
    }
    h4 {
        font-weight: 300;
        margin: 0;
    }
`;

export const Image = styled.div`
    width: 80px;
    height: 80px;
    border-radius: 50%;
    border: 4px #fff solid;

    img {
        width: 100%;
    }
`;
