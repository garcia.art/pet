import React from 'react';
import * as S from './styled.js';
import { Container } from '../../components/Container/index.js';
import Card from '../../components/Card';
import Button from '../../components/Button';
import { pricing } from '../../data/pricing';

export default function LPricing() {
    return (
        <>
            <S.Cover />
            <S.Wrapper>
                <Container>
                    <h1>Pricing</h1>
                    <p>LEARN MORE ABOUT OUR TEAM</p>
                    <S.CardWrapper>
                        {pricing.map(({ src, title, price, content }) => (
                            <Card typeCard="price">
                                <img src={src}></img>
                                <h2>{title}</h2>
                                <h3>
                                    <sup>R$</sup>
                                    {price}
                                    <span>/ MÊS</span>
                                </h3>
                                {content}
                                <Button typeButton="price">Contratar</Button>
                            </Card>
                        ))}
                    </S.CardWrapper>
                </Container>
            </S.Wrapper>
        </>
    );
}
