import styled from 'styled-components';

export const Wrapper = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    text-align: center;
    padding: 70px 15px 3em 15px;

    color: #fff;
    h1 {
        font-size: 2.4em;
        line-height: 2.4em;
    }

    p {
        letter-spacing: 1.4px;
        font-weight: 500;
        color: rgba(255, 255, 255, 0.6);
    }
    @media (max-width: 800px) {
        background: #877bf5;
    }
`;

export const CardWrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    margin: 5em 0em;

    @media (min-width: 800px) {
        justify-content: space-between;
    }

    @media (min-width: 1030px) {
        justify-content: space-around;
    }
`;

export const Cover = styled.div`
    z-index: -2;
    height: 600px;
    width: 100%;
    position: absolute;
    display: none;
    background: #877bf5;

    @media (min-width: 800px) {
        display: flex;
    }
`;

export const Control = styled.div`
    display: flex;
    justify-content: space-between;

    @media (min-width: 800px) {
        justify-content: space-between;
    }
`;
