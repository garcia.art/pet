import React from 'react';
import * as S from './styled';
import Link from 'next/link';
import Card from '../../components/Card';
import { Container } from '../../components/Container';
import { funcoes } from '../../data/funcoes';
export default function Funcoes() {
    return (
        <S.Wrapper>
            <Container>
                <S.Content>
                    <h1>A melhor experiência de gestão</h1>
                    <p>
                        A descriptive if there is anyone who loves or pursues or desires to obtain
                        pain and anything else to finish this sentence.
                    </p>
                </S.Content>
                <S.CardWrapper>
                    {funcoes.map(({ src, h4, p }) => (
                        <Card typeCard="home">
                            <img src={src}></img>

                            <h4>{h4}</h4>
                            <p>{p}</p>
                        </Card>
                    ))}
                </S.CardWrapper>
                <S.More>
                    <Link href="#">
                        <a>VER TODAS</a>
                    </Link>
                </S.More>
            </Container>
        </S.Wrapper>
    );
}
