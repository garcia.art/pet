import styled from 'styled-components';

export const Wrapper = styled.div`
    width: 100%;
    display: flex;
    text-align: center;
    padding: 3em 0;
`;

export const Content = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    p {
        color: #676f79;
        margin-top: 1em;
        max-width: 450px;
    }
    h1 {
        color: #333333;
    }

    @media (min-width: 700px) {
        p {
            margin: 1em 0 2em 0;
        }
    }
`;

export const CardWrapper = styled.div`
    display: flex;
    width: 100%;
    flex-wrap: wrap;
    justify-content: center;

    @media (min-width: 1000px) {
        margin: 1em 0;

        div {
            border: thin solid #fff;
        }

        div:hover {
            border: thin solid #35a4ff;
        }
    }
`;

export const More = styled.div`
    a {
        color: #7265e3;
        text-decoration: none;
        text-transform: uppercase;
        font-weight: 600;
        line-height: 2em;
        letter-spacing: 0.144375px;
    }
`;
