import React from 'react';
import * as S from './styled.js';
import B1 from '../../public/bg1.svg';
import B2 from '../../public/bg2.svg';
import Img from '../../public/homeImage.png';
import Button from '../../components/Button';
import { Container } from '../../components/Container';

export default function Landing() {
    return (
        <S.Wrapper>
            <S.Cover src={B1}></S.Cover>
            <S.Cover2 src={B2}></S.Cover2>
            <Container>
                <S.Content>
                    <S.Text>
                        <h1>Sistema de Gestão PET completo e totalmente online</h1>
                        <h3>
                            I must explain to you how all this mistaken idea of denouncing pleasure
                            and praising pain was born.
                        </h3>
                        <Button typeButton="home1">TESTE GRÁTIS</Button>
                    </S.Text>
                    <S.Image>
                        <img src={Img}></img>
                    </S.Image>
                </S.Content>
            </Container>
        </S.Wrapper>
    );
}
