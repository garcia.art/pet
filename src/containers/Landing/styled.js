import styled from 'styled-components';

export const Wrapper = styled.div`
    width: 100%;
    height: 100vh;
    padding-top: 70px;
    display: flex;
    justify-content: center;
    align-items: center;
    background: #877bf5;
`;

export const Cover = styled.img`
    z-index: -10;
    position: absolute;
    top: 0;
    right: 0;
    width: 100%;
    height: 100%;
    display: none;
    /* SUMIU */
    @media (min-width: 1200px) {
    }
`;

export const Cover2 = styled.img`
    z-index: -10;
    position: absolute;
    top: 0;
    right: 0;
    width: 100%;
    display: none;
    /* SUMIU */
    @media (max-width: 1200px) {
    }
`;

export const Content = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    @media (min-width: 1050px) {
        flex-direction: row;
    }
`;

export const Text = styled.div`
    width: 100%;
    height: 80%;
    color: #ffffff;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    max-width: 600px;
    h1 {
        font-weight: 600;
        font-size: 2.2em;
        line-height: 1.4em;
        text-align: center;
        margin-bottom: 1rem;
    }

    h3 {
        font-weight: 300;
        font-size: 1.3em;
        line-height: 1.3em;
        text-align: center;
    }

    @media (min-width: 800px) {
        h1 {
            font-size: 3em;
        }

        h3 {
            font-size: 2em;
        }
    }

    @media (min-width: 1200px) {
        width: 30%;
        align-items: initial;
        h1 {
            font-size: 2.2em;
            text-align: left;
        }

        h3 {
            font-size: 1.3em;
            text-align: left;
        }
    }
    @media (max-height: 600px) {
        height: 100%;
    }
`;

export const Image = styled.div`
    width: 100%;
    display: none;

    img {
        width: 100%;
    }

    @media (min-width: 1050px) {
        width: 70%;
        position: absolute;
        right: 0;
        img{
            width: 100%;
        }
       
    }
    }
    @media (min-height: 600px) {
        display: flex;
        
`;
