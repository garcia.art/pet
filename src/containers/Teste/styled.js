import styled from 'styled-components';

export const Wrapper = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    text-align: center;
    padding: 3em 15px;

    h1 {
        font-size: 1.5em;
        text-transform: uppercase;
        color: #333;
        margin: 0;
    }

    p {
        font-size: 1.2em;
        line-height: 1.5em;
        color: #676f79;
        margin: 1em;
    }

    @media (min-width: 1000px) {
        height: 330px;

        h1 {
            font-size: 1.7em;
        }
        p {
            width: initial;
        }
    }
`;
