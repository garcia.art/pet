import React from 'react';
import * as S from './styled.js';
import Button from '../../components/Button';

export default function Teste() {
    return (
        <S.Wrapper>
            <h1>Comece Agora Mesmo</h1>
            <p>If you are interested in my services and would like additional information.</p>
            <Button typeButton="home2">Iniciar Teste</Button>
        </S.Wrapper>
    );
}
